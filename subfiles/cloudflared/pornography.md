## How many % of pornographic domains are using Cloudflare?


We downloaded the pornhosts list from [here](https://mypdns.org/my-privacy-dns/porn-records) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 9,173 |
| net | 1,129 |
| org | 314 |
| pro | 246 |
| tv | 214 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 5,784 |
| Normal | 8,321 |


### 41% of pornographic domains are using Cloudflare.