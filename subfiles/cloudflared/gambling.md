## How many % of gambling domains are using Cloudflare?


We downloaded the gambling list from [here](https://raw.githubusercontent.com/Sinfonietta/hostfiles/master/gambling-hosts) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 986 |
| eu | 47 |
| net | 43 |
| ag | 39 |
| uk | 34 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 611 |
| Normal | 798 |


### 43.4% of gambling domains are using Cloudflare.