## How many % of fake news outlets are using Cloudflare?


- [Fake News](https://en.wikipedia.org/wiki/Fake_news)
```
Fake news is false or misleading information presented as news.
It often has the aim of damaging the reputation of a person or entity, or making money through advertising revenue.
However, the term does not have a fixed definition, and has been applied more broadly to include any type of false information, 
including unintentional and unconscious mechanisms, and also by high-profile individuals to apply to any news unfavourable 
to their personal perspectives. 
```

We downloaded the fake news list from [here](https://raw.githubusercontent.com/marktron/fakenews/master/fakenews) and filter out duplicates.
Here's the result.


[//]: # (start replacement)


- Top 5 TLDs

| TLD | Count |
| --- | --- |
| com | 1,807 |
| news | 216 |
| org | 52 |
| net | 27 |
| info | 11 |


- Cloudflare %

| Type | Count |
| --- | --- |
| Cloudflare | 536 |
| Normal | 1,635 |


### 24.7% of fake news outlets are using Cloudflare.