## How many % of Hosting Providers are using Cloudflare on their site?


- [Hosting Provider](https://en.wikipedia.org/wiki/Web_hosting_service)
```
A web hosting service (often shortened to web host) is a type of Internet hosting service 
that allows individuals and organizations to make their website accessible via the World Wide Web. 
Web hosts are companies that provide space on a server owned or leased for use by clients, 
as well as providing Internet connectivity, typically in a data center.
```


The following is a list of Hosting Providers.


| Name | Site | Cloudflared |
| --- | --- | --- |
| 1and1.com | https://www.ionos.com/ | No |
| 1und1.de | https://1und1.de/ | No |
| 22.cn | https://22.cn/ | No |
| 123-reg.co.uk | https://123-reg.co.uk/ | No |
| 1984 Hosting | https://www.1984hosting.com/ | No |
| a2hosting.com | https://a2hosting.com/ | Yes |
| above.com | https://above.com/ | No |
| afternic.com | https://afternic.com/ | No |
| alidns.com | https://alidns.com/ | No |
| alwaysdata | https://www.alwaysdata.com/ | No |
| Antagonist | https://www.antagonist.nl/ | No |
| Aquatis | https://aquatis.host/ | Yes |
| beget.com | https://beget.com/ | No |
| beget.pro | https://beget.pro/ | No |
| bluehost.com | https://bluehost.com/ | Yes |
| bodis.com | https://bodis.com/ | No |
| bplaced | https://www.bplaced.net/ | No |
| buydomains.com | https://buydomains.com/ | Yes |
| CA Net, Europe | https://www.canet.at/ | No |
| cashparking.com | https://cashparking.com/ | No |
| cdns.cn | https://cdns.cn/ | No |
| Clausweb | https://www.clausweb.ro/ | No |
| cloudflare.com | https://cloudflare.com/ | No |
| Combell | https://www.combell.com/ | Yes |
| Conetix | https://conetix.com.au/ | No |
| cscdns | https://cscdns.net/ | No |
| Cyon.ch | https://www.cyon.ch/ | No |
| dan.com | https://dan.com/ | Yes |
| digitalocean.com | https://digitalocean.com/ | Yes |
| dns-parking.com | https://dns-parking.com/ | Yes |
| dns.com | https://dns.com/ | No |
| dns.ne.jp | https://dns.ne.jp/ | No |
| dnsdun.com | https://dnsdun.com/ | No |
| dnsdun | https://dnsdun.net/ | No |
| dnsimple.com | https://dnsimple.com/ | Yes |
| dnsmadeeasy.com | https://dnsmadeeasy.com/ | No |
| dnsnw.com | https://dnsnw.com/ | No |
| dnsowl.com | https://dnsowl.com/ | Yes |
| dnspod | https://dnspod.net/ | No |
| dnsv.jp | https://dnsv.jp/ | No |
| domain.com | https://domain.com/ | No |
| domaincontrol.com | https://domaincontrol.com/ | No |
| Domainnameshop | https://www.domainnameshop.com/ | No |
| DreamHost | https://www.dreamhost.com/ | Yes |
| dynadot.com | https://dynadot.com/ | No |
| dynect | https://dynect.net/ | No |
| easyname | https://www.easyname.com/ | No |
| EasyStore | https://www.easystore.co/ | No |
| epik.com | https://epik.com/ | No |
| EvergreenTech.io | https://evergreentech.io/ | Yes |
| Firebase Hosting | https://firebase.google.com/ | No |
| Futureweb OG | https://www.futureweb.at/ | No |
| gandi | https://gandi.net/ | Yes |
| GitLab | https://about.gitlab.com/ | Yes |
| gname-dns.com | https://gname-dns.com/ | No |
| googledomains.com | https://googledomains.com/ | No |
| gratisdns.dk | https://gratisdns.dk/ | No |
| HahuCloud | https://www.hahucloud.com/ | Yes |
| hichina.com | https://hichina.com/ | No |
| hosteurope.de | https://hosteurope.de/ | No |
| hostgator.com | https://hostgator.com/ | Yes |
| HostPapa | https://www.hostpapa.com/ | Yes |
| hover.com | https://hover.com/ | Yes |
| hugedomainsdns.com | https://hugedomainsdns.com/ | Yes |
| hyp | https://hyp.net/ | No |
| ICDSoft | https://www.icdsoft.com/ | No |
| ikoula | https://www.ikoula.com/ | No |
| Infomaniak | https://www.infomaniak.com/ | No |
| inmotionhosting.com | https://inmotionhosting.com/ | Yes |
| Inspedium Corp. | https://www.inspedium.com/ | Yes |
| internettraffic.com | https://internettraffic.com/ | No |
| Jimdo | https://www.jimdo.com/ | Yes |
| kasserver.com | https://kasserver.com/ | No |
| livedns.co.uk | https://livedns.co.uk/ | No |
| maff.com | https://maff.com/ | No |
| manitu | https://www.manitu.de/ | No |
| Marketing One | https://www.marketingone.com.au/ | No |
| markmonitor.com | https://markmonitor.com/ | No |
| maxcluster | https://maxcluster.de/ | No |
| microsoftonline.com | https://microsoftonline.com/ | No |
| mijn.host | https://mijn.host/ | No |
| MyDevil.net | https://www.mydevil.net/ | No |
| MyDreams innovations s.r.o. | https://www.mydreams.cz/ | Yes |
| myhostadmin | https://myhostadmin.net/ | No |
| Mythic Beasts | https://www.mythic-beasts.com/ | No |
| nagor.cn | https://nagor.cn/ | No |
| nagor.com.cn | https://nagor.com.cn/ | No |
| name-services.com | https://name-services.com/ | No |
| name.com | https://name.com/ | Yes |
| namebrightdns.com | https://namebrightdns.com/ | Yes |
| Namecheap.com | https://www.namecheap.com/ | Yes |
| namecheaphosting.com | https://namecheaphosting.com/ | No |
| namedynamics | https://namedynamics.net/ | No |
| namefind.com | https://namefind.com/ | No |
| namespace4you.de | https://namespace4you.de/ | No |
| natrohost.com | https://natrohost.com/ | No |
| NDCHost | https://ndchost.com/ | No |
| NearlyFreeSpeech.NET | https://www.nearlyfreespeech.net/ | No |
| NederHost | https://www.nederhost.nl/ | No |
| Neocities | https://neocities.org/ | No |
| netclusive | https://netclusive.de/ | No |
| Netlify | https://www.netlify.com/ | Yes |
| Netsite | https://www.netsite.dk/ | No |
| nic.ru | https://nic.ru/ | No |
| nsone | https://nsone.net/ | Yes |
| Nuclear.Hosting | https://nuclear.hosting/ | No |
| one.com | https://www.one.com/ | Yes |
| orderbox-dns.com | https://orderbox-dns.com/ | Yes |
| OVHcloud | https://www.ovh.ie/ | No |
| ovh | https://ovh.net/ | No |
| Pantheon | https://pantheon.io/ | Yes |
| parkingcrew | https://parkingcrew.net/ | No |
| parklogic.com | https://parklogic.com/ | Yes |
| PlanetHoster Inc | https://www.planethoster.com/ | No |
| Platform.sh | https://platform.sh/ | No |
| porkbun.com | https://porkbun.com/ | No |
| Pride Tech Design | https://pridetechdesign.com/ | No |
| QTH.com Hosting | https://hosting.qth.com/ | No |
| Rad Web Hosting | https://radwebhosting.com/ | No |
| reg.ru | https://reg.ru/ | No |
| register.com | https://register.com/ | Yes |
| registrar-servers.com | https://registrar-servers.com/ | No |
| Render | https://render.com/ | Yes |
| rzone.de | https://rzone.de/ | No |
| sav.com | https://sav.com/ | Yes |
| schlund.de | https://schlund.de/ | No |
| schlundtech.de | https://schlundtech.de/ | No |
| sedoparking.com | https://sedoparking.com/ | No |
| Seravo | https://seravo.com/ | No |
| ServerPilot | https://serverpilot.io/ | No |
| Shellrent | https://www.shellrent.com/ | Yes |
| signaltransmitter.de | https://signaltransmitter.de/ | Yes |
| SiteGround.com | https://www.siteground.com/ | No |
| siteground | https://siteground.net/ | No |
| SnapPages | https://www.snappages.net/ | No |
| Spry Servers | https://www.spryservers.net/ | Yes |
| squarespacedns.com | https://squarespacedns.com/ | No |
| Squarespace | https://www.squarespace.com/ | Yes |
| systemdns.com | https://systemdns.com/ | No |
| technorail.com | https://technorail.com/ | No |
| Thecamels | https://thecamels.org/ | No |
| Thexyz | https://www.thexyz.com/ | No |
| this-domain-for-sale.com | https://this-domain-for-sale.com/ | Yes |
| Uberspace | https://uberspace.de/ | No |
| udagdns | https://udagdns.net/ | No |
| ui-dns.biz | https://ui-dns.biz/ | No |
| ui-dns.com | https://ui-dns.com/ | No |
| ui-dns.de | https://ui-dns.de/ | No |
| ui-dns.org | https://ui-dns.org/ | No |
| Umbler | https://www.umbler.com/ | Yes |
| uniregistrymarket.link | https://uniregistrymarket.link/ | No |
| value-domain.com | https://value-domain.com/ | Yes |
| VeeroTech | https://www.veerotech.net/ | Yes |
| Vero Servers | https://www.veroservers.com/ | Yes |
| Vimexx | https://www.vimexx.nl/ | No |
| Vélhost | https://velhost.fr/ | No |
| Webdock | https://webdock.io/ | Yes |
| webkeeper.ch | https://www.webkeeper.ch/ | No |
| WebOké | https://www.weboke.nl/ | No |
| websitewelcome.com | https://websitewelcome.com/ | Yes |
| Wikstrom Telephone Company | https://wiktel.com/ | No |
| wixdns | https://wixdns.net/ | No |
| WordPress.com | https://wordpress.com/ | No |
| worldnic.com | https://worldnic.com/ | Yes |
| WRX Online | https://wrx.hu/ | Yes |
| xincache.com | https://xincache.com/ | No |
| XMission | https://www.xmission.com/ | No |
| xserver.jp | https://xserver.jp/ | No |
| xundns.com | https://xundns.com/ | No |
| yahoo.com | https://yahoo.com/ | No |
| Zoner Oy | https://www.zoner.fi/ | Yes |
| ztomy.com | https://ztomy.com/ | No |


-----

| Type | Count |
| --- | --- | 
| Cloudflare | 49 |
| Normal | 123 |


### 28.49% of Hosting Providers are using Cloudflare on their site.
