## How many % of adverts and trackers are using Cloudflare?


- [Easylist](https://web.archive.org/web/20210516110248/https://easylist.to/)
```
EasyList is the primary filter list that removes most adverts from international webpages, including unwanted frames, images and objects.

EasyPrivacy is an optional supplementary filter list that completely removes all forms of tracking from the internet, including web bugs, tracking scripts and information collectors, thereby protecting your personal data.
```


We picked domain-blocking lines from the list and filtered out domains which has exception rules.
Here's the result.


| Adblock list | Domains Count | Cloudflare | % |
| --- | --- | --- | --- |
| [EasyList](https://easylist.to/easylist/easylist.txt) | 27,915 | 7,941 | 28.4% |
| [EasyPrivacy](https://easylist.to/easylist/easyprivacy.txt) | 16,558 | 4,980 | 30.1% |
| [AdGuard](https://adguardteam.github.io/AdGuardSDNSFilter/Filters/filter.txt) | 43,852 | 8,572 | 19.5% |
| [AdAway](https://raw.githubusercontent.com/AdAway/adaway.github.io/master/hosts.txt) | 2,054 | 682 | 33.2% |
| Total | 61,883 | 15,896 | 25.7% |


### 25.7% of adverts and trackers are using Cloudflare.